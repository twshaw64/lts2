﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LTSLogic;
using LTSXML;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter username: ");
            string username = Console.ReadLine();
            UserDetails user = UserDetails.Load(username);
            Console.Write("Enter new password: ");
            string plainPassword = Console.ReadLine();
            user.PlainTextPassword = plainPassword;
            user.Save();
            Console.WriteLine("Done");
            Console.ReadKey();
        }

        private static bool tryagain()
        {
            Console.WriteLine("Try again?");
            return Console.ReadKey(true).KeyChar == 'y';
        }
    }
}
