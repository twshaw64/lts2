﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LTSXML;
using LTSLogic;

namespace LazyTownSports
{
    public partial class Management_EditUser : Form
    {
        #region Members and Fields
        private UserDetails mUser;

        public UserDetails GetUser()
        {
            return mUser;
        }
        #endregion

        #region Constructors
        public Management_EditUser()
        {
            InitializeComponent();
            comboUserType.Text = "Gym Member";
        }
        public Management_EditUser(UserDetails pUser,bool Mangement): this()
        {
            txtEditUsername.ReadOnly = true;
            txtEditUsername.Text = pUser.Username;
            txtEditPassword.Text = pUser.HashedPassword;
            txtEditPhoneNumber.Text = pUser.PhoneNumber;
            txtEditAddressLine1.Text = pUser.AddressLine1;
            txtEditCity.Text = pUser.City;
            txtEditCounty.Text = pUser.County;
            txtEditPostCode.Text = pUser.PostCode;
            txtFirstName.Text = pUser.FirstName;
            txtSurname.Text = pUser.Surname;
            txtEditEmailAddress.Text = pUser.Email;
            comboUserType.Text = pUser.UserLevel.ToString();
            checkBoxActiveUser.Checked = pUser.ActiveUser;
            mUser = pUser;
           if(Mangement == false)
            {
                comboUserType.Enabled = false;
                checkBoxActiveUser.Enabled = false;
            }
        }
        #endregion

        #region Event Handlers
        #region Cancel Button
        private void btnCancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region Clear Button
        private void btnClear_Click(object sender, EventArgs e)
        {
            if (mUser != null)
            {
                txtEditUsername.Text = mUser.Username;
                txtEditPassword.Text = mUser.HashedPassword;
                txtEditPhoneNumber.Text = mUser.PhoneNumber;
                txtEditAddressLine1.Text = mUser.AddressLine1;
                txtEditCity.Text = mUser.City;
                txtEditCounty.Text = mUser.County;
                txtEditPostCode.Text = mUser.PostCode;
                txtFirstName.Text = mUser.FirstName;
                txtSurname.Text = mUser.Surname;
                txtEditEmailAddress.Text = mUser.Email;
                comboUserType.Text = mUser.UserLevel.ToString();
                checkBoxActiveUser.Checked = mUser.ActiveUser;
            }
            else
            {
                txtEditUsername.Clear();
                txtEditPassword.Clear();
                txtEditPhoneNumber.Clear();
                txtEditAddressLine1.Clear();
                txtEditCity.Clear();
                txtEditCounty.Clear();
                txtEditPostCode.Clear();
                txtFirstName.Clear();
                txtSurname.Clear();
                txtEditEmailAddress.Clear();
                checkBoxActiveUser.Checked = false;
            }

        }
        #endregion

        #region Apply Changes
        private void btnApplyChanges_Click(object sender, EventArgs e)
        {
            if (mUser == null)
            {
                try
                {
                    mUser = new UserDetails(txtEditUsername.Text, txtEditPassword.Text, false, txtEditPhoneNumber.Text, txtEditAddressLine1.Text, txtEditCity.Text, txtEditCounty.Text, txtEditPostCode.Text, txtEditEmailAddress.Text, txtFirstName.Text, txtSurname.Text, StringToUserLevel.StringToEnum(comboUserType.Text), checkBoxActiveUser.Checked);
                    mUser.Save();
                }
                catch (UserDetailException ex)
                {
                    string exceptionMessage = ex.Message;
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                }
            }
            else
            {
                try
                {
                    if(mUser.HashedPassword != txtEditPassword.Text)
                    {
                        mUser.PlainTextPassword = txtEditPassword.Text;
                    }
                    mUser.PhoneNumber = txtEditPhoneNumber.Text;
                    mUser.AddressLine1 = txtEditAddressLine1.Text;
                    mUser.City = txtEditCity.Text;
                    mUser.County = txtEditCounty.Text;
                    mUser.PostCode = txtEditPostCode.Text;
                    mUser.FirstName = txtFirstName.Text;
                    mUser.Surname = txtSurname.Text;
                    mUser.Email = txtEditEmailAddress.Text;
                    mUser.UserLevel = StringToUserLevel.StringToEnum(comboUserType.Text);
                    mUser.ActiveUser = checkBoxActiveUser.Checked;
                    mUser.Save();

                    this.Close();
                }
                catch (UserDetailException ex)
                {
                    string exceptionMessage = ex.Message;
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                }
            }
            this.Close();

        }
        #endregion
        #endregion
    }
}
