﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml.Linq;
using LTSLogic;
using LTSXML;
using System.IO;

namespace TestingProject
{
    [TestClass]
    public class UnitTest
    {
        #region Global Test Values
        static string testUsername = "testing";
        static string plainTextTestPassword = "password";
        static string testFirstName = "michael";
        static string testSurname = "watson";
        static string testAddressLine1 = "20 cranbrook avenue";
        static string testCity = "hull";
        static string testCounty = "east yorkshire";
        static string testPostCode = "hu65sn";
        static string testPhoneNumber = "07888888888";
        static string testEmail = "w@w.com";
        static UserLevel testUserLevel = UserLevel.GymMember;
        static bool testActiveMember = true;
        static UserDetails testDetails = new UserDetails(testUsername, plainTextTestPassword, false, testPhoneNumber, testAddressLine1, testCity, testCounty, testPostCode, testEmail, testFirstName, testSurname, testUserLevel, testActiveMember);
        #endregion

        #region Login Validation Test
        [TestMethod]
        public void LoginValidation()
        {
            //test values
            string testUsername = "testing";
            string plainTextTestPassword = "password";
            string testFirstName = "michael";
            string testSurname = "watson";
            string testAddressLine1 = "20 cranbrook avenue";
            string testCity = "hull";
            string testCounty = "east yorkshire";
            string testPostCode = "hu65sn";
            string testPhoneNumber = "07888888888";
            string testEmail = "w@w.com";
            UserLevel testUserLevel = UserLevel.GymMember;
            bool testActiveMember = true;

            //generate Test XML file
            string hashedPassword = new HashString().HashedString(plainTextTestPassword);
            XDocument doc = new XDocument();
            XElement element = new XElement("logins",
                new XElement("login",
                    new XElement("username", testUsername),
                    new XElement("password", hashedPassword),
                    new XElement("firstName", testFirstName),
                    new XElement("surname", testSurname),
                    new XElement("addressLine1", testAddressLine1),
                    new XElement("city", testCity),
                    new XElement("county", testCounty),
                    new XElement("postCode", testPostCode),
                    new XElement("phoneNumber", testPhoneNumber),
                    new XElement("email", testEmail),
                    new XElement("userLevel", testUserLevel.ToString()),
                    new XElement("activeMember", testActiveMember)));
            doc.Add(element);
            string filePath = StorageLocation.Logins;
            doc.Save(filePath);

            UserDetails testDetails = UserDetails.Load(testUsername);

            //run test
            ValidateLogin validate = new ValidateLogin(testUsername, plainTextTestPassword);
            UserDetails validatedUser = validate.Validate();
            Assert.AreEqual(testDetails.FirstName, validatedUser.FirstName);
        }
        #endregion

        #region UserDetails
        #region Username
        [TestMethod]
        public void UserDetailsUsername()
        {
            //test data
            string[] shouldFailTestData = new string[] { "", " ", "h e", "k l k" };
            string[] shouldPassTestData = new string[] { "test", "hello2" };

            //run test

            foreach (string test in shouldFailTestData)
            {
                try
                {
                    testDetails.Username = test;
                    throw new AssertFailedException(test);
                }
                catch (UserDetailException)
                {

                }
            }
            foreach (string test in shouldPassTestData)
            {
                testDetails.Username = test;
            }
            Assert.IsTrue(true);
        }
        #endregion
        #region First Name
        [TestMethod]
        public void UserDetailsFirstName()
        {
            //test data
            string[] shouldFail = new string[] { "", " ", "1", "h3", "h h" };
            string[] shouldPass = new string[] { "test", "Test", "tEST", "TEST" };

            //run test
            foreach (string test in shouldFail)
            {
                try
                {
                    testDetails.FirstName = test;
                    throw new AssertFailedException(test);
                }
                catch (UserDetailException) { }
            }
            foreach(string test in shouldPass)
            {
                testDetails.FirstName = test;
            }
            Assert.IsTrue(true);
        }
        #endregion
        #region Surname
        public void UserDetailsSurname()
        {
            //test data
            string[] shouldFail = new string[] { "", " ", "1", "h2", "h h" };
            string[] shouldPass = new string[] { "test", "TEST", "Test", "tEST" };

            //run test
            foreach(string test in shouldFail)
            {
                try
                {
                    testDetails.Surname = test;
                    throw new AssertFailedException(test);
                }
                catch (UserDetailException) { }
            }
            foreach(string test in shouldPass)
            {
                testDetails.Surname = test;
            }
            Assert.IsTrue(true);
        }
        #endregion
        #region PostCode
        [TestMethod]
        public void UserDetailsPostCode()
        {
            //test data
            string[] shouldFail = new string[] { "", " ", "gg", "H4", "55t", "hhhhhhhhhhhh" };
            string[] shouldPass = new string[] { "hhhhhh", "HHHHHHH", "hHhHhHhH", "666666", "6666hhh" };

            //run test
            foreach(string test in shouldFail)
            {
                try
                {
                    testDetails.PostCode = test;
                    throw new AssertFailedException(test);
                }
                catch (UserDetailException) { }
            }
            foreach(string test in shouldPass)
            {
                testDetails.PostCode = test;
            }
            Assert.IsTrue(true);
        }
        #endregion
        #region Email Address
        [TestMethod]
        public void UserDetailsEmailAddress()
        {
            //test data
            string[] shouldFail = new string[] { "", " ", "test", "test@test", "test.com", "test@", "@test", ".test", "test.", "test@." };
            string[] shouldPass = new string[] { "test@testserver.com", "test@me.co.uk", "test.test@me.com", "test.test@test.co.uk" };

            //run test
            foreach(string test in shouldFail)
            {
                try
                {
                    testDetails.Email = test;
                    throw new AssertFailedException(test);
                }
                catch (UserDetailException) { }
            }
            foreach(string test in shouldPass)
            {
                testDetails.Email = test;
            }
            Assert.IsTrue(true);
        }
        #endregion
        #region Phone Number
        [TestMethod]
        public void UserDetailsPhoneNumber()
        {
            //test data
            string[] shouldFail = new string[] { "", " ", "h", "0", "000000000000", "hhhhhhhhhhh","99999999999" };
            string[] shouldPass = new string[] { "07888888888", "07123456789", "01248636256" }; 

            //run test
            foreach(string test in shouldFail)
            {
                try
                {
                    testDetails.PhoneNumber = test;
                    throw new AssertFailedException(test);
                }
                catch (UserDetailException) { }
            }
            foreach(string test in shouldPass)
            {
                testDetails.PhoneNumber = test;
            }
            Assert.IsTrue(true);
        }
        #endregion
        #endregion
    }
}
