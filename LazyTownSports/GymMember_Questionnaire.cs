﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LTSXML;

namespace LazyTownSports
{
    public partial class GymMember_Questionnaire : Form
    {
        private UserDetails CurrentUser;

        public GymMember_Questionnaire(UserDetails pCurrentUser)
        {
            InitializeComponent();
            CurrentUser = pCurrentUser;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSubmitValues_Click(object sender, EventArgs e)
        {
            DialogResult msg = MessageBox.Show("Are you sure you want to submit these values? You won't be able to change your personal trainer afterwards!", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);

            if (msg == DialogResult.Yes)
            {
                if (trackBarMotivated.Value <= 3 && trackBarFitness.Value >= 8)
                {
                    MessageBox.Show("You have been assigned: PERSONAL_TRAINER_1", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
                if (trackBarMotivated.Value <= 6 && trackBarFitness.Value >= 8)
                {
                    MessageBox.Show("You have been assigned: PERSONAL_TRAINER_2", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
                if (trackBarMotivated.Value <= 10 && trackBarFitness.Value >= 8)
                {
                    MessageBox.Show("You have been assigned: PERSONAL_TRAINER_3", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
                if (trackBarMotivated.Value <= 3 && trackBarFitness.Value >= 5)
                {
                    MessageBox.Show("You have been assigned: PERSONAL_TRAINER_4", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
                if (trackBarMotivated.Value <= 6 && trackBarFitness.Value >= 5)
                {
                    MessageBox.Show("You have been assigned: PERSONAL_TRAINER_5", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
                if (trackBarMotivated.Value <= 10 && trackBarFitness.Value >= 5)
                {
                    MessageBox.Show("You have been assigned: PERSONAL_TRAINER_6", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
                if (trackBarMotivated.Value <= 6 && trackBarFitness.Value >= 1)
                {
                    MessageBox.Show("You have been assigned: PERSONAL_TRAINER_7", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
                if (trackBarMotivated.Value <= 3 && trackBarFitness.Value >= 1)
                {
                    MessageBox.Show("You have been assigned: PERSONAL_TRAINER_8", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
                if (trackBarMotivated.Value <= 10 && trackBarFitness.Value >= 1)
                {
                    MessageBox.Show("You have been assigned: PERSONAL_TRAINER_9", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                }
            }
        }
    }
}
