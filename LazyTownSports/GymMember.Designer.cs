﻿namespace LazyTownSports
{
    partial class GymMember
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GymMember));
            this.TabManagement = new System.Windows.Forms.TabControl();
            this.tabBooking = new System.Windows.Forms.TabPage();
            this.labelBookingDate = new System.Windows.Forms.Label();
            this.listBoxDisplayUnbookedTimes = new System.Windows.Forms.ListBox();
            this.dtPickerBooking = new System.Windows.Forms.DateTimePicker();
            this.checkBoxMUGA = new System.Windows.Forms.CheckBox();
            this.checkBoxLargeRoom = new System.Windows.Forms.CheckBox();
            this.checkBoxMediumRoom = new System.Windows.Forms.CheckBox();
            this.checkBoxSmallRoom = new System.Windows.Forms.CheckBox();
            this.btnBook = new System.Windows.Forms.Button();
            this.tabTimetable = new System.Windows.Forms.TabPage();
            this.labelTimetableDate = new System.Windows.Forms.Label();
            this.listBoxDisplayBookedTimes = new System.Windows.Forms.ListBox();
            this.dtPickerTimetable = new System.Windows.Forms.DateTimePicker();
            this.btnCancelSelected = new System.Windows.Forms.Button();
            this.btnCreateNote = new System.Windows.Forms.Button();
            this.tabManageUsers = new System.Windows.Forms.TabPage();
            this.labelPersonalTrainerName = new System.Windows.Forms.Label();
            this.labelPersonalTrainerDeclaration = new System.Windows.Forms.Label();
            this.btnQuestionnaire = new System.Windows.Forms.Button();
            this.btnContactPersonalTrainer = new System.Windows.Forms.Button();
            this.tabCurrentEvents = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.labelEventsDate = new System.Windows.Forms.Label();
            this.listBoxDisplayEvents = new System.Windows.Forms.ListBox();
            this.dtPickerEvents = new System.Windows.Forms.DateTimePicker();
            this.btnSignUpForEvent = new System.Windows.Forms.Button();
            this.tabReportProblem = new System.Windows.Forms.TabPage();
            this.labelReportProblemFooter = new System.Windows.Forms.Label();
            this.btnSubmitReport = new System.Windows.Forms.Button();
            this.txtBoxReportProblem = new System.Windows.Forms.TextBox();
            this.labelReportProblemSubtitle = new System.Windows.Forms.Label();
            this.labelReportProblem = new System.Windows.Forms.Label();
            this.statusStripGymMember = new System.Windows.Forms.StatusStrip();
            this.stripLabelGymMember = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabManageAccount = new System.Windows.Forms.TabPage();
            this.TabManagement.SuspendLayout();
            this.tabBooking.SuspendLayout();
            this.tabTimetable.SuspendLayout();
            this.tabManageUsers.SuspendLayout();
            this.tabCurrentEvents.SuspendLayout();
            this.tabReportProblem.SuspendLayout();
            this.statusStripGymMember.SuspendLayout();
            this.SuspendLayout();
            // 
            // TabManagement
            // 
            this.TabManagement.Controls.Add(this.tabBooking);
            this.TabManagement.Controls.Add(this.tabTimetable);
            this.TabManagement.Controls.Add(this.tabManageUsers);
            this.TabManagement.Controls.Add(this.tabCurrentEvents);
            this.TabManagement.Controls.Add(this.tabManageAccount);
            this.TabManagement.Controls.Add(this.tabReportProblem);
            this.TabManagement.Location = new System.Drawing.Point(1, 1);
            this.TabManagement.Multiline = true;
            this.TabManagement.Name = "TabManagement";
            this.TabManagement.SelectedIndex = 0;
            this.TabManagement.Size = new System.Drawing.Size(521, 461);
            this.TabManagement.TabIndex = 1;
            this.TabManagement.SelectedIndexChanged += new System.EventHandler(this.TabManagement_SelectedIndexChanged);
            // 
            // tabBooking
            // 
            this.tabBooking.Controls.Add(this.labelBookingDate);
            this.tabBooking.Controls.Add(this.listBoxDisplayUnbookedTimes);
            this.tabBooking.Controls.Add(this.dtPickerBooking);
            this.tabBooking.Controls.Add(this.checkBoxMUGA);
            this.tabBooking.Controls.Add(this.checkBoxLargeRoom);
            this.tabBooking.Controls.Add(this.checkBoxMediumRoom);
            this.tabBooking.Controls.Add(this.checkBoxSmallRoom);
            this.tabBooking.Controls.Add(this.btnBook);
            this.tabBooking.Location = new System.Drawing.Point(4, 22);
            this.tabBooking.Name = "tabBooking";
            this.tabBooking.Padding = new System.Windows.Forms.Padding(3);
            this.tabBooking.Size = new System.Drawing.Size(513, 435);
            this.tabBooking.TabIndex = 0;
            this.tabBooking.Text = "Booking";
            this.tabBooking.UseVisualStyleBackColor = true;
            // 
            // labelBookingDate
            // 
            this.labelBookingDate.AutoSize = true;
            this.labelBookingDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBookingDate.Location = new System.Drawing.Point(65, 91);
            this.labelBookingDate.Name = "labelBookingDate";
            this.labelBookingDate.Size = new System.Drawing.Size(36, 15);
            this.labelBookingDate.TabIndex = 20;
            this.labelBookingDate.Text = "Date:";
            // 
            // listBoxDisplayUnbookedTimes
            // 
            this.listBoxDisplayUnbookedTimes.FormattingEnabled = true;
            this.listBoxDisplayUnbookedTimes.Location = new System.Drawing.Point(68, 135);
            this.listBoxDisplayUnbookedTimes.Name = "listBoxDisplayUnbookedTimes";
            this.listBoxDisplayUnbookedTimes.Size = new System.Drawing.Size(200, 160);
            this.listBoxDisplayUnbookedTimes.TabIndex = 8;
            // 
            // dtPickerBooking
            // 
            this.dtPickerBooking.Location = new System.Drawing.Point(68, 109);
            this.dtPickerBooking.Name = "dtPickerBooking";
            this.dtPickerBooking.Size = new System.Drawing.Size(200, 20);
            this.dtPickerBooking.TabIndex = 7;
            this.dtPickerBooking.ValueChanged += new System.EventHandler(this.dtPickerBooking_ValueChanged);
            // 
            // checkBoxMUGA
            // 
            this.checkBoxMUGA.AutoSize = true;
            this.checkBoxMUGA.Location = new System.Drawing.Point(299, 219);
            this.checkBoxMUGA.Name = "checkBoxMUGA";
            this.checkBoxMUGA.Size = new System.Drawing.Size(172, 17);
            this.checkBoxMUGA.TabIndex = 6;
            this.checkBoxMUGA.Text = "Multi-Use Games Area (MUGA)";
            this.checkBoxMUGA.UseVisualStyleBackColor = true;
            this.checkBoxMUGA.CheckedChanged += new System.EventHandler(this.checkBoxBookingType4_CheckedChanged);
            // 
            // checkBoxLargeRoom
            // 
            this.checkBoxLargeRoom.AutoSize = true;
            this.checkBoxLargeRoom.Location = new System.Drawing.Point(299, 185);
            this.checkBoxLargeRoom.Name = "checkBoxLargeRoom";
            this.checkBoxLargeRoom.Size = new System.Drawing.Size(84, 17);
            this.checkBoxLargeRoom.TabIndex = 5;
            this.checkBoxLargeRoom.Text = "Large Room";
            this.checkBoxLargeRoom.UseVisualStyleBackColor = true;
            this.checkBoxLargeRoom.CheckedChanged += new System.EventHandler(this.checkBoxBookingType3_CheckedChanged);
            // 
            // checkBoxMediumRoom
            // 
            this.checkBoxMediumRoom.AutoSize = true;
            this.checkBoxMediumRoom.Location = new System.Drawing.Point(299, 150);
            this.checkBoxMediumRoom.Name = "checkBoxMediumRoom";
            this.checkBoxMediumRoom.Size = new System.Drawing.Size(94, 17);
            this.checkBoxMediumRoom.TabIndex = 4;
            this.checkBoxMediumRoom.Text = "Medium Room";
            this.checkBoxMediumRoom.UseVisualStyleBackColor = true;
            this.checkBoxMediumRoom.CheckedChanged += new System.EventHandler(this.checkBoxBookingType2_CheckedChanged);
            // 
            // checkBoxSmallRoom
            // 
            this.checkBoxSmallRoom.AutoSize = true;
            this.checkBoxSmallRoom.Location = new System.Drawing.Point(299, 116);
            this.checkBoxSmallRoom.Name = "checkBoxSmallRoom";
            this.checkBoxSmallRoom.Size = new System.Drawing.Size(82, 17);
            this.checkBoxSmallRoom.TabIndex = 3;
            this.checkBoxSmallRoom.Text = "Small Room";
            this.checkBoxSmallRoom.UseVisualStyleBackColor = true;
            this.checkBoxSmallRoom.CheckedChanged += new System.EventHandler(this.checkBoxBookingType1_CheckedChanged);
            // 
            // btnBook
            // 
            this.btnBook.Location = new System.Drawing.Point(288, 254);
            this.btnBook.Name = "btnBook";
            this.btnBook.Size = new System.Drawing.Size(124, 41);
            this.btnBook.TabIndex = 1;
            this.btnBook.Text = "Book";
            this.btnBook.UseVisualStyleBackColor = true;
            this.btnBook.Click += new System.EventHandler(this.btnBook_Click);
            this.btnBook.MouseLeave += new System.EventHandler(this.btnBook_MouseLeave);
            this.btnBook.MouseHover += new System.EventHandler(this.btnBook_MouseHover);
            // 
            // tabTimetable
            // 
            this.tabTimetable.Controls.Add(this.labelTimetableDate);
            this.tabTimetable.Controls.Add(this.listBoxDisplayBookedTimes);
            this.tabTimetable.Controls.Add(this.dtPickerTimetable);
            this.tabTimetable.Controls.Add(this.btnCancelSelected);
            this.tabTimetable.Controls.Add(this.btnCreateNote);
            this.tabTimetable.Location = new System.Drawing.Point(4, 22);
            this.tabTimetable.Name = "tabTimetable";
            this.tabTimetable.Padding = new System.Windows.Forms.Padding(3);
            this.tabTimetable.Size = new System.Drawing.Size(513, 435);
            this.tabTimetable.TabIndex = 1;
            this.tabTimetable.Text = "Timetable";
            this.tabTimetable.UseVisualStyleBackColor = true;
            // 
            // labelTimetableDate
            // 
            this.labelTimetableDate.AutoSize = true;
            this.labelTimetableDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTimetableDate.Location = new System.Drawing.Point(82, 89);
            this.labelTimetableDate.Name = "labelTimetableDate";
            this.labelTimetableDate.Size = new System.Drawing.Size(36, 15);
            this.labelTimetableDate.TabIndex = 19;
            this.labelTimetableDate.Text = "Date:";
            // 
            // listBoxDisplayBookedTimes
            // 
            this.listBoxDisplayBookedTimes.FormattingEnabled = true;
            this.listBoxDisplayBookedTimes.Location = new System.Drawing.Point(85, 133);
            this.listBoxDisplayBookedTimes.Name = "listBoxDisplayBookedTimes";
            this.listBoxDisplayBookedTimes.Size = new System.Drawing.Size(200, 160);
            this.listBoxDisplayBookedTimes.TabIndex = 9;
            // 
            // dtPickerTimetable
            // 
            this.dtPickerTimetable.Location = new System.Drawing.Point(85, 107);
            this.dtPickerTimetable.Name = "dtPickerTimetable";
            this.dtPickerTimetable.Size = new System.Drawing.Size(200, 20);
            this.dtPickerTimetable.TabIndex = 8;
            this.dtPickerTimetable.Value = new System.DateTime(2018, 12, 4, 0, 0, 0, 0);
            this.dtPickerTimetable.ValueChanged += new System.EventHandler(this.dtPickerTimetable_ValueChanged);
            // 
            // btnCancelSelected
            // 
            this.btnCancelSelected.Location = new System.Drawing.Point(312, 207);
            this.btnCancelSelected.Name = "btnCancelSelected";
            this.btnCancelSelected.Size = new System.Drawing.Size(142, 48);
            this.btnCancelSelected.TabIndex = 3;
            this.btnCancelSelected.Text = "Cancel Selected";
            this.btnCancelSelected.UseVisualStyleBackColor = true;
            this.btnCancelSelected.Click += new System.EventHandler(this.btnCancelSelected_Click);
            this.btnCancelSelected.MouseLeave += new System.EventHandler(this.btnCancelSelected_MouseLeave);
            this.btnCancelSelected.MouseHover += new System.EventHandler(this.btnCancelSelected_MouseHover);
            // 
            // btnCreateNote
            // 
            this.btnCreateNote.Location = new System.Drawing.Point(312, 153);
            this.btnCreateNote.Name = "btnCreateNote";
            this.btnCreateNote.Size = new System.Drawing.Size(142, 48);
            this.btnCreateNote.TabIndex = 2;
            this.btnCreateNote.Text = "Create Note";
            this.btnCreateNote.UseVisualStyleBackColor = true;
            this.btnCreateNote.Click += new System.EventHandler(this.btnCreateNote_Click);
            this.btnCreateNote.MouseLeave += new System.EventHandler(this.btnCreateNote_MouseLeave);
            this.btnCreateNote.MouseHover += new System.EventHandler(this.btnCreateNote_MouseHover_1);
            // 
            // tabManageUsers
            // 
            this.tabManageUsers.Controls.Add(this.labelPersonalTrainerName);
            this.tabManageUsers.Controls.Add(this.labelPersonalTrainerDeclaration);
            this.tabManageUsers.Controls.Add(this.btnQuestionnaire);
            this.tabManageUsers.Controls.Add(this.btnContactPersonalTrainer);
            this.tabManageUsers.Location = new System.Drawing.Point(4, 22);
            this.tabManageUsers.Name = "tabManageUsers";
            this.tabManageUsers.Padding = new System.Windows.Forms.Padding(3);
            this.tabManageUsers.Size = new System.Drawing.Size(513, 435);
            this.tabManageUsers.TabIndex = 2;
            this.tabManageUsers.Text = "Personal Trainer";
            this.tabManageUsers.UseVisualStyleBackColor = true;
            // 
            // labelPersonalTrainerName
            // 
            this.labelPersonalTrainerName.AutoSize = true;
            this.labelPersonalTrainerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPersonalTrainerName.Location = new System.Drawing.Point(259, 33);
            this.labelPersonalTrainerName.Name = "labelPersonalTrainerName";
            this.labelPersonalTrainerName.Size = new System.Drawing.Size(69, 16);
            this.labelPersonalTrainerName.TabIndex = 3;
            this.labelPersonalTrainerName.Text = "Sportacus";
            // 
            // labelPersonalTrainerDeclaration
            // 
            this.labelPersonalTrainerDeclaration.AutoSize = true;
            this.labelPersonalTrainerDeclaration.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPersonalTrainerDeclaration.Location = new System.Drawing.Point(20, 27);
            this.labelPersonalTrainerDeclaration.Name = "labelPersonalTrainerDeclaration";
            this.labelPersonalTrainerDeclaration.Size = new System.Drawing.Size(233, 24);
            this.labelPersonalTrainerDeclaration.TabIndex = 2;
            this.labelPersonalTrainerDeclaration.Text = "Your personal trainer is:";
            // 
            // btnQuestionnaire
            // 
            this.btnQuestionnaire.Location = new System.Drawing.Point(336, 356);
            this.btnQuestionnaire.Name = "btnQuestionnaire";
            this.btnQuestionnaire.Size = new System.Drawing.Size(140, 31);
            this.btnQuestionnaire.TabIndex = 1;
            this.btnQuestionnaire.Text = "Questionnaire";
            this.btnQuestionnaire.UseVisualStyleBackColor = true;
            this.btnQuestionnaire.Click += new System.EventHandler(this.btnQuestionnaire_Click);
            // 
            // btnContactPersonalTrainer
            // 
            this.btnContactPersonalTrainer.Location = new System.Drawing.Point(24, 356);
            this.btnContactPersonalTrainer.Name = "btnContactPersonalTrainer";
            this.btnContactPersonalTrainer.Size = new System.Drawing.Size(140, 31);
            this.btnContactPersonalTrainer.TabIndex = 0;
            this.btnContactPersonalTrainer.Text = "Contact Personal Trainer";
            this.btnContactPersonalTrainer.UseVisualStyleBackColor = true;
            // 
            // tabCurrentEvents
            // 
            this.tabCurrentEvents.Controls.Add(this.label1);
            this.tabCurrentEvents.Controls.Add(this.labelEventsDate);
            this.tabCurrentEvents.Controls.Add(this.listBoxDisplayEvents);
            this.tabCurrentEvents.Controls.Add(this.dtPickerEvents);
            this.tabCurrentEvents.Controls.Add(this.btnSignUpForEvent);
            this.tabCurrentEvents.Location = new System.Drawing.Point(4, 22);
            this.tabCurrentEvents.Name = "tabCurrentEvents";
            this.tabCurrentEvents.Padding = new System.Windows.Forms.Padding(3);
            this.tabCurrentEvents.Size = new System.Drawing.Size(513, 435);
            this.tabCurrentEvents.TabIndex = 3;
            this.tabCurrentEvents.Text = "Events";
            this.tabCurrentEvents.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(159, 106);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 15);
            this.label1.TabIndex = 20;
            this.label1.Text = "Events:";
            // 
            // labelEventsDate
            // 
            this.labelEventsDate.AutoSize = true;
            this.labelEventsDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEventsDate.Location = new System.Drawing.Point(159, 60);
            this.labelEventsDate.Name = "labelEventsDate";
            this.labelEventsDate.Size = new System.Drawing.Size(36, 15);
            this.labelEventsDate.TabIndex = 19;
            this.labelEventsDate.Text = "Date:";
            // 
            // listBoxDisplayEvents
            // 
            this.listBoxDisplayEvents.FormattingEnabled = true;
            this.listBoxDisplayEvents.Location = new System.Drawing.Point(162, 124);
            this.listBoxDisplayEvents.Name = "listBoxDisplayEvents";
            this.listBoxDisplayEvents.Size = new System.Drawing.Size(200, 160);
            this.listBoxDisplayEvents.TabIndex = 9;
            // 
            // dtPickerEvents
            // 
            this.dtPickerEvents.Location = new System.Drawing.Point(162, 78);
            this.dtPickerEvents.Name = "dtPickerEvents";
            this.dtPickerEvents.Size = new System.Drawing.Size(200, 20);
            this.dtPickerEvents.TabIndex = 8;
            // 
            // btnSignUpForEvent
            // 
            this.btnSignUpForEvent.Location = new System.Drawing.Point(187, 302);
            this.btnSignUpForEvent.Name = "btnSignUpForEvent";
            this.btnSignUpForEvent.Size = new System.Drawing.Size(141, 34);
            this.btnSignUpForEvent.TabIndex = 0;
            this.btnSignUpForEvent.Text = "Sign up for event";
            this.btnSignUpForEvent.UseVisualStyleBackColor = true;
            this.btnSignUpForEvent.Click += new System.EventHandler(this.btnSignUpForEvent_Click);
            this.btnSignUpForEvent.MouseLeave += new System.EventHandler(this.btnSignUpForEvent_MouseLeave);
            this.btnSignUpForEvent.MouseHover += new System.EventHandler(this.btnSignUpForEvent_MouseHover);
            // 
            // tabReportProblem
            // 
            this.tabReportProblem.Controls.Add(this.labelReportProblemFooter);
            this.tabReportProblem.Controls.Add(this.btnSubmitReport);
            this.tabReportProblem.Controls.Add(this.txtBoxReportProblem);
            this.tabReportProblem.Controls.Add(this.labelReportProblemSubtitle);
            this.tabReportProblem.Controls.Add(this.labelReportProblem);
            this.tabReportProblem.Location = new System.Drawing.Point(4, 22);
            this.tabReportProblem.Name = "tabReportProblem";
            this.tabReportProblem.Padding = new System.Windows.Forms.Padding(3);
            this.tabReportProblem.Size = new System.Drawing.Size(513, 435);
            this.tabReportProblem.TabIndex = 5;
            this.tabReportProblem.Text = "Report Problem";
            this.tabReportProblem.UseVisualStyleBackColor = true;
            // 
            // labelReportProblemFooter
            // 
            this.labelReportProblemFooter.AutoSize = true;
            this.labelReportProblemFooter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelReportProblemFooter.Location = new System.Drawing.Point(33, 309);
            this.labelReportProblemFooter.Name = "labelReportProblemFooter";
            this.labelReportProblemFooter.Size = new System.Drawing.Size(453, 15);
            this.labelReportProblemFooter.TabIndex = 9;
            this.labelReportProblemFooter.Text = "Please remember to be as descriptive as possible about the situation!";
            // 
            // btnSubmitReport
            // 
            this.btnSubmitReport.Location = new System.Drawing.Point(175, 348);
            this.btnSubmitReport.Name = "btnSubmitReport";
            this.btnSubmitReport.Size = new System.Drawing.Size(135, 44);
            this.btnSubmitReport.TabIndex = 8;
            this.btnSubmitReport.Text = "Submit Report";
            this.btnSubmitReport.UseVisualStyleBackColor = true;
            this.btnSubmitReport.Click += new System.EventHandler(this.btnSubmitReport_Click);
            // 
            // txtBoxReportProblem
            // 
            this.txtBoxReportProblem.Location = new System.Drawing.Point(46, 98);
            this.txtBoxReportProblem.Multiline = true;
            this.txtBoxReportProblem.Name = "txtBoxReportProblem";
            this.txtBoxReportProblem.Size = new System.Drawing.Size(408, 194);
            this.txtBoxReportProblem.TabIndex = 7;
            // 
            // labelReportProblemSubtitle
            // 
            this.labelReportProblemSubtitle.Location = new System.Drawing.Point(11, 54);
            this.labelReportProblemSubtitle.Name = "labelReportProblemSubtitle";
            this.labelReportProblemSubtitle.Size = new System.Drawing.Size(498, 32);
            this.labelReportProblemSubtitle.TabIndex = 6;
            this.labelReportProblemSubtitle.Text = resources.GetString("labelReportProblemSubtitle.Text");
            // 
            // labelReportProblem
            // 
            this.labelReportProblem.AutoSize = true;
            this.labelReportProblem.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelReportProblem.Location = new System.Drawing.Point(6, 21);
            this.labelReportProblem.Name = "labelReportProblem";
            this.labelReportProblem.Size = new System.Drawing.Size(200, 25);
            this.labelReportProblem.TabIndex = 5;
            this.labelReportProblem.Text = "Report a problem:";
            // 
            // statusStripGymMember
            // 
            this.statusStripGymMember.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.statusStripGymMember.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stripLabelGymMember});
            this.statusStripGymMember.Location = new System.Drawing.Point(0, 465);
            this.statusStripGymMember.Name = "statusStripGymMember";
            this.statusStripGymMember.Size = new System.Drawing.Size(534, 22);
            this.statusStripGymMember.TabIndex = 2;
            this.statusStripGymMember.Text = "statusStrip1";
            // 
            // stripLabelGymMember
            // 
            this.stripLabelGymMember.Name = "stripLabelGymMember";
            this.stripLabelGymMember.Size = new System.Drawing.Size(118, 17);
            this.stripLabelGymMember.Text = "toolStripStatusLabel1";
            // 
            // tabManageAccount
            // 
            this.tabManageAccount.Location = new System.Drawing.Point(4, 22);
            this.tabManageAccount.Name = "tabManageAccount";
            this.tabManageAccount.Padding = new System.Windows.Forms.Padding(3);
            this.tabManageAccount.Size = new System.Drawing.Size(513, 435);
            this.tabManageAccount.TabIndex = 4;
            this.tabManageAccount.Text = "Manage Account";
            this.tabManageAccount.UseVisualStyleBackColor = true;
            // 
            // GymMember
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(534, 487);
            this.Controls.Add(this.statusStripGymMember);
            this.Controls.Add(this.TabManagement);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "GymMember";
            this.Text = "LTS | Gym Member Portal";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.GymMember_FormClosing);
            this.Load += new System.EventHandler(this.GymMember_Load);
            this.TabManagement.ResumeLayout(false);
            this.tabBooking.ResumeLayout(false);
            this.tabBooking.PerformLayout();
            this.tabTimetable.ResumeLayout(false);
            this.tabTimetable.PerformLayout();
            this.tabManageUsers.ResumeLayout(false);
            this.tabManageUsers.PerformLayout();
            this.tabCurrentEvents.ResumeLayout(false);
            this.tabCurrentEvents.PerformLayout();
            this.tabReportProblem.ResumeLayout(false);
            this.tabReportProblem.PerformLayout();
            this.statusStripGymMember.ResumeLayout(false);
            this.statusStripGymMember.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl TabManagement;
        private System.Windows.Forms.TabPage tabBooking;
        private System.Windows.Forms.TabPage tabTimetable;
        private System.Windows.Forms.TabPage tabManageUsers;
        private System.Windows.Forms.TabPage tabCurrentEvents;
        private System.Windows.Forms.Button btnBook;
        private System.Windows.Forms.Button btnCancelSelected;
        private System.Windows.Forms.Button btnCreateNote;
        private System.Windows.Forms.Button btnSignUpForEvent;
        private System.Windows.Forms.CheckBox checkBoxMUGA;
        private System.Windows.Forms.CheckBox checkBoxLargeRoom;
        private System.Windows.Forms.CheckBox checkBoxMediumRoom;
        private System.Windows.Forms.CheckBox checkBoxSmallRoom;
        private System.Windows.Forms.StatusStrip statusStripGymMember;
        private System.Windows.Forms.ToolStripStatusLabel stripLabelGymMember;
        private System.Windows.Forms.DateTimePicker dtPickerBooking;
        private System.Windows.Forms.DateTimePicker dtPickerTimetable;
        private System.Windows.Forms.DateTimePicker dtPickerEvents;
        private System.Windows.Forms.ListBox listBoxDisplayUnbookedTimes;
        private System.Windows.Forms.ListBox listBoxDisplayEvents;
        private System.Windows.Forms.ListBox listBoxDisplayBookedTimes;
        private System.Windows.Forms.Label labelEventsDate;
        private System.Windows.Forms.Label labelTimetableDate;
        private System.Windows.Forms.Label labelBookingDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabReportProblem;
        private System.Windows.Forms.Label labelReportProblemFooter;
        private System.Windows.Forms.Button btnSubmitReport;
        private System.Windows.Forms.TextBox txtBoxReportProblem;
        private System.Windows.Forms.Label labelReportProblemSubtitle;
        private System.Windows.Forms.Label labelReportProblem;
        private System.Windows.Forms.Button btnQuestionnaire;
        private System.Windows.Forms.Button btnContactPersonalTrainer;
        private System.Windows.Forms.Label labelPersonalTrainerName;
        private System.Windows.Forms.Label labelPersonalTrainerDeclaration;
        private System.Windows.Forms.TabPage tabManageAccount;
    }
}