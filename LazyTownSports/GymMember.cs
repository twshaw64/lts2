﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LTSXML;

namespace LazyTownSports
{
    public partial class GymMember : Form
    {
        private UserDetails CurrentUser;
        public List<RoomBooking> RoomBookings = new List<RoomBooking>();

        public GymMember(UserDetails pCurrentUser)
        {
            InitializeComponent();
            CurrentUser = pCurrentUser;

            labelPersonalTrainerName.Text = "No personal trainer!";
        }
        private void UpdateBookings()
        {
            
        }
        

        private void GymMember_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult closeResult = MessageBox.Show("Are you sure you want to log out?", "Close?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

            if (closeResult == DialogResult.No)
            {
                e.Cancel = true;
                return;
            }
            
            Login login = new Login();
            login.Show();
        }

        private void checkBoxBookingType1_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxMediumRoom.Checked = false;
            checkBoxLargeRoom.Checked = false;
            checkBoxMUGA.Checked = false;
        }

        private void checkBoxBookingType2_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxSmallRoom.Checked = false;
            checkBoxLargeRoom.Checked = false;
            checkBoxMUGA.Checked = false;
        }

        private void checkBoxBookingType3_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxSmallRoom.Checked = false;
            checkBoxMediumRoom.Checked = false;
            checkBoxMUGA.Checked = false;
        }

        private void checkBoxBookingType4_CheckedChanged(object sender, EventArgs e)
        {
            checkBoxSmallRoom.Checked = false;
            checkBoxMediumRoom.Checked = false;
            checkBoxLargeRoom.Checked = false;
        }

        private void btnCreateNote_Click(object sender, EventArgs e)
        {
            Timetable_CreateNote form = new Timetable_CreateNote();
            form.ShowDialog();
        }

        private void btnBook_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Are you sure you want to book this slot?", "PLACEHOLDER BUTTON PROMPT", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        private void btnBook_MouseLeave(object sender, EventArgs e)
        {
            stripLabelGymMember.Text = "";
        }

        private void btnBook_MouseHover(object sender, EventArgs e)
        {
            stripLabelGymMember.Text = "Book the selected slot in the calendar.";
        }

        private void btnCreateNote_MouseHover_1(object sender, EventArgs e)
        {
            stripLabelGymMember.Text = "Create a note for the selected date.";
        }

        private void btnCreateNote_MouseLeave(object sender, EventArgs e)
        {
            stripLabelGymMember.Text = "";
        }

        private void btnCancelSelected_Click(object sender, EventArgs e)
        {
            stripLabelGymMember.Text = "The selected appointments have been cancelled.";
        }

        private void btnCancelSelected_MouseHover(object sender, EventArgs e)
        {
            stripLabelGymMember.Text = "Cancel the selected appointments on the calendar.";
        }

        private void btnCancelSelected_MouseLeave(object sender, EventArgs e)
        {
            stripLabelGymMember.Text = "";
        }

        private void btnSignUpForEvent_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Are you sure you want to sign up for this event?", "PLACEHOLDER BUTTON PROMPT", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        private void btnSignUpForEvent_MouseHover(object sender, EventArgs e)
        {
            stripLabelGymMember.Text = "Sign up for the selected event.";
        }

        private void btnSignUpForEvent_MouseLeave(object sender, EventArgs e)
        {
            stripLabelGymMember.Text = "";
        }


        private void GymMember_Load(object sender, EventArgs e)
        {
            stripLabelGymMember.Text = $"Welcome {CurrentUser.FirstName} {CurrentUser.Surname}!";
            

            if (labelPersonalTrainerName.Text == "No personal trainer!")
            {
                btnContactPersonalTrainer.Enabled = false;
            }
        }

        /// <summary>
        /// Populate the listbox with all events booked by the current user on the selected date
        /// </summary>
        private void PopulateEventsBox()
        {
            List<Booking> bookings = new List<Booking>();
            List<EventBooking> eventBookings = null;
            try
            {
                eventBookings = EventBooking.Load(CurrentUser.Username, dtPickerTimetable.Value.Date);
                foreach (EventBooking b in eventBookings)
                {
                    bookings.Add(b);
                }
            }
            catch (BookingNotFoundException) { }
            List<PTBooking> ptBookings = null;
            try
            {
                ptBookings = PTBooking.Load(CurrentUser.Username, dtPickerTimetable.Value.Date);
                foreach (PTBooking b in ptBookings)
                {
                    bookings.Add(b);
                }
            }
            catch (BookingNotFoundException) { }
            List<RoomBooking> roomBookings = null;
            try
            {
                roomBookings = RoomBooking.Load(CurrentUser.Username, dtPickerTimetable.Value.Date);
                foreach (RoomBooking b in roomBookings)
                {
                    bookings.Add(b);
                }
            }
            catch (BookingNotFoundException) { }
            bookings.Sort();
            listBoxDisplayBookedTimes.Items.Clear();
            foreach(Booking b in bookings)
            {
                listBoxDisplayBookedTimes.Items.Add(b);
            }
        }

        private void btnQuestionnaire_Click(object sender, EventArgs e)
        {
            GymMember_Questionnaire qstnForm = new GymMember_Questionnaire(CurrentUser);
            qstnForm.ShowDialog();
        }

        private void btnSubmitReport_Click(object sender, EventArgs e)
        {
            int reportID = Report.NextID;
            DateTime date = DateTime.Now;
            string reportingUsername = CurrentUser.Username;
            string description = txtBoxReportProblem.Text;
            Report report = new Report(reportID, date, reportingUsername, description, false);
            report.Save();
            txtBoxReportProblem.Text = "";
        }

        private void dtPickerTimetable_ValueChanged(object sender, EventArgs e)
        {
            PopulateEventsBox();
        }

        private void dtPickerBooking_ValueChanged(object sender, EventArgs e)
        {

        }

        private void TabManagement_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Checks if the tab changes to the manage account tab and pulls up the edit user form
            if (TabManagement.SelectedTab.Name == "tabManageAccount")
            {
                TabManagement.SelectedIndex = 0;
                UserDetails User = CurrentUser;
                if (User != null)
                {
                    Management_EditUser form = new Management_EditUser(User, false);
                    form.ShowDialog();
                    if (form.DialogResult == DialogResult.OK)
                    {
                        stripLabelGymMember.Text = $"Welcome {CurrentUser.FirstName} {CurrentUser.Surname}!";
                        //if the user is updated so is the strip label as they might have changed their name 
                    }
                }
                

            }
        }
    }
}
