﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LTSXML;

namespace LazyTownSports
{
    public partial class PersonalTrainer : Form
    {
        private UserDetails CurrentUser;

        public PersonalTrainer(UserDetails pCurrentUser)
        {
            InitializeComponent();
            CurrentUser = pCurrentUser;
        }

        private void PersonalTrainer_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult closeResult = MessageBox.Show("Are you sure you want to log out?", "Close?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);

            if (closeResult == DialogResult.No)
            {
                e.Cancel = true;
            }
			
			this.Hide();
			Login form = new Login();
			form.Show();
        }

        private void btnBook_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Are you sure you want to book this slot?", "PLACEHOLDER BUTTON PROMPT", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        private void btnBook_MouseMove(object sender, MouseEventArgs e)
        {
            stripLabelPersonalTrainer.Text = "Book the selected slot in the calendar.";
        }

        private void btnBook_MouseLeave(object sender, EventArgs e)
        {
            stripLabelPersonalTrainer.Text = "";
        }

        private void btnCreateNote_Click(object sender, EventArgs e)
        {
            Timetable_CreateNote form = new Timetable_CreateNote();
            form.ShowDialog();
        }

        private void btnCreateNote_MouseHover(object sender, EventArgs e)
        {
            stripLabelPersonalTrainer.Text = "Create a note for the selected date.";
        }

        private void btnCreateNote_MouseLeave(object sender, EventArgs e)
        {
            stripLabelPersonalTrainer.Text = "";
        }

        private void btnCancelSelected_Click(object sender, EventArgs e)
        {
            stripLabelPersonalTrainer.Text = "The selected appointments have been cancelled.";
        }

        private void btnCancelSelected_MouseHover(object sender, EventArgs e)
        {
            stripLabelPersonalTrainer.Text = "Cancel the selected appointments on the calendar.";
        }

        private void btnCancelSelected_MouseLeave(object sender, EventArgs e)
        {
            stripLabelPersonalTrainer.Text = "";
        }

        private void btnSignUpForEvent_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Are you sure you want to sign up for this event?", "PLACEHOLDER BUTTON PROMPT", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        }

        private void btnSignUpForEvent_MouseHover(object sender, EventArgs e)
        {
            stripLabelPersonalTrainer.Text = "Sign up for the selected event.";
        }

        private void btnSignUpForEvent_MouseLeave(object sender, EventArgs e)
        {
            stripLabelPersonalTrainer.Text = "";
        }

        

        private void panelTimetableCalendar_Paint(object sender, PaintEventArgs e)
        {

        }

        private void labelMyEvents_Click(object sender, EventArgs e)
        {

        }

        private void btnBook_MouseHover(object sender, EventArgs e)
        {
            stripLabelPersonalTrainer.Text = "Book the selected time at the gym.";
        }

        private void btnBook_MouseLeave_1(object sender, EventArgs e)
        {
            stripLabelPersonalTrainer.Text = "";
        }

        private void PersonalTrainer_Load(object sender, EventArgs e)
        {
            stripLabelPersonalTrainer.Text = $"Welcome {CurrentUser.FirstName} {CurrentUser.Surname}";
        }

        private void TabManagement_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Checks if the tab changes to the manage account tab and pulls up the edit user form
            if (TabManagement.SelectedTab.Name == "tabManagement")
            {
                TabManagement.SelectedIndex = 0;
                UserDetails User = CurrentUser;
                if (User != null)
                {
                    Management_EditUser form = new Management_EditUser(User, false);
                    form.ShowDialog();
                    if (form.DialogResult == DialogResult.OK)
                    {
                        stripLabelPersonalTrainer.Text = $"Welcome {CurrentUser.FirstName} {CurrentUser.Surname}!";
                        //if the user is updated so is the strip label as they might have changed their name 
                    }
                }

            }
        }
    }
}

