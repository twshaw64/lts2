﻿namespace LazyTownSports
{
    partial class PersonalTrainer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PersonalTrainer));
            this.TabManagement = new System.Windows.Forms.TabControl();
            this.tabBooking = new System.Windows.Forms.TabPage();
            this.labelBookingDate = new System.Windows.Forms.Label();
            this.listBoxDisplayUnbookedTimes = new System.Windows.Forms.ListBox();
            this.dtPickerBooking = new System.Windows.Forms.DateTimePicker();
            this.checkBoxMUGA = new System.Windows.Forms.CheckBox();
            this.checkBoxLargeRoom = new System.Windows.Forms.CheckBox();
            this.checkBoxMediumRoom = new System.Windows.Forms.CheckBox();
            this.checkBoxSmallRoom = new System.Windows.Forms.CheckBox();
            this.btnBook = new System.Windows.Forms.Button();
            this.tabTimetable = new System.Windows.Forms.TabPage();
            this.labelTimetableDate = new System.Windows.Forms.Label();
            this.listBoxDisplayBookedTimes = new System.Windows.Forms.ListBox();
            this.dtPickerTimetable = new System.Windows.Forms.DateTimePicker();
            this.btnPersonalTrainerCancelSelected = new System.Windows.Forms.Button();
            this.btnPersonalTrainerCreateNote = new System.Windows.Forms.Button();
            this.btnCancelSelected = new System.Windows.Forms.Button();
            this.btnCreateNote = new System.Windows.Forms.Button();
            this.tabManageClients = new System.Windows.Forms.TabPage();
            this.labelManageClientsSearch = new System.Windows.Forms.Label();
            this.txtBoxManageClientsSearch = new System.Windows.Forms.TextBox();
            this.labelListOfClients = new System.Windows.Forms.Label();
            this.listBoxUsers = new System.Windows.Forms.ListBox();
            this.tabCurrentEvents = new System.Windows.Forms.TabPage();
            this.labelCurrentEventsEvents = new System.Windows.Forms.Label();
            this.labelEventsDate = new System.Windows.Forms.Label();
            this.listBoxDisplayEvents = new System.Windows.Forms.ListBox();
            this.dtPickerEvents = new System.Windows.Forms.DateTimePicker();
            this.btnSignUpForEvent = new System.Windows.Forms.Button();
            this.tabMyEvents = new System.Windows.Forms.TabPage();
            this.labelMyEventsSearch = new System.Windows.Forms.Label();
            this.txtBoxMyEventsSearch = new System.Windows.Forms.TextBox();
            this.labelMyEvents = new System.Windows.Forms.Label();
            this.listBoxMyEvents = new System.Windows.Forms.ListBox();
            this.tabManageAccount = new System.Windows.Forms.TabPage();
            this.tabReportProblem = new System.Windows.Forms.TabPage();
            this.labelReportProblemFooter = new System.Windows.Forms.Label();
            this.btnSubmitReport = new System.Windows.Forms.Button();
            this.txtBoxReportProblem = new System.Windows.Forms.TextBox();
            this.labelReportProblemSubtitle = new System.Windows.Forms.Label();
            this.labelReportProblem = new System.Windows.Forms.Label();
            this.statusStripPersonalTrainer = new System.Windows.Forms.StatusStrip();
            this.personalTrainer_StripLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.stripLabelPersonalTrainer = new System.Windows.Forms.ToolStripStatusLabel();
            this.TabManagement.SuspendLayout();
            this.tabBooking.SuspendLayout();
            this.tabTimetable.SuspendLayout();
            this.tabManageClients.SuspendLayout();
            this.tabCurrentEvents.SuspendLayout();
            this.tabMyEvents.SuspendLayout();
            this.tabReportProblem.SuspendLayout();
            this.statusStripPersonalTrainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // TabManagement
            // 
            this.TabManagement.Controls.Add(this.tabBooking);
            this.TabManagement.Controls.Add(this.tabTimetable);
            this.TabManagement.Controls.Add(this.tabManageClients);
            this.TabManagement.Controls.Add(this.tabCurrentEvents);
            this.TabManagement.Controls.Add(this.tabMyEvents);
            this.TabManagement.Controls.Add(this.tabManageAccount);
            this.TabManagement.Controls.Add(this.tabReportProblem);
            this.TabManagement.Location = new System.Drawing.Point(4, 4);
            this.TabManagement.Multiline = true;
            this.TabManagement.Name = "TabManagement";
            this.TabManagement.SelectedIndex = 0;
            this.TabManagement.Size = new System.Drawing.Size(558, 462);
            this.TabManagement.TabIndex = 2;
            this.TabManagement.SelectedIndexChanged += new System.EventHandler(this.TabManagement_SelectedIndexChanged);
            // 
            // tabBooking
            // 
            this.tabBooking.Controls.Add(this.labelBookingDate);
            this.tabBooking.Controls.Add(this.listBoxDisplayUnbookedTimes);
            this.tabBooking.Controls.Add(this.dtPickerBooking);
            this.tabBooking.Controls.Add(this.checkBoxMUGA);
            this.tabBooking.Controls.Add(this.checkBoxLargeRoom);
            this.tabBooking.Controls.Add(this.checkBoxMediumRoom);
            this.tabBooking.Controls.Add(this.checkBoxSmallRoom);
            this.tabBooking.Controls.Add(this.btnBook);
            this.tabBooking.Location = new System.Drawing.Point(4, 22);
            this.tabBooking.Name = "tabBooking";
            this.tabBooking.Padding = new System.Windows.Forms.Padding(3);
            this.tabBooking.Size = new System.Drawing.Size(550, 436);
            this.tabBooking.TabIndex = 0;
            this.tabBooking.Text = "Booking";
            this.tabBooking.UseVisualStyleBackColor = true;
            // 
            // labelBookingDate
            // 
            this.labelBookingDate.AutoSize = true;
            this.labelBookingDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBookingDate.Location = new System.Drawing.Point(72, 102);
            this.labelBookingDate.Name = "labelBookingDate";
            this.labelBookingDate.Size = new System.Drawing.Size(36, 15);
            this.labelBookingDate.TabIndex = 19;
            this.labelBookingDate.Text = "Date:";
            // 
            // listBoxDisplayUnbookedTimes
            // 
            this.listBoxDisplayUnbookedTimes.FormattingEnabled = true;
            this.listBoxDisplayUnbookedTimes.Location = new System.Drawing.Point(75, 146);
            this.listBoxDisplayUnbookedTimes.Name = "listBoxDisplayUnbookedTimes";
            this.listBoxDisplayUnbookedTimes.Size = new System.Drawing.Size(200, 160);
            this.listBoxDisplayUnbookedTimes.TabIndex = 15;
            // 
            // dtPickerBooking
            // 
            this.dtPickerBooking.Location = new System.Drawing.Point(75, 120);
            this.dtPickerBooking.Name = "dtPickerBooking";
            this.dtPickerBooking.Size = new System.Drawing.Size(200, 20);
            this.dtPickerBooking.TabIndex = 14;
            // 
            // checkBoxMUGA
            // 
            this.checkBoxMUGA.AutoSize = true;
            this.checkBoxMUGA.Location = new System.Drawing.Point(306, 230);
            this.checkBoxMUGA.Name = "checkBoxMUGA";
            this.checkBoxMUGA.Size = new System.Drawing.Size(172, 17);
            this.checkBoxMUGA.TabIndex = 13;
            this.checkBoxMUGA.Text = "Multi-Use Games Area (MUGA)";
            this.checkBoxMUGA.UseVisualStyleBackColor = true;
            // 
            // checkBoxLargeRoom
            // 
            this.checkBoxLargeRoom.AutoSize = true;
            this.checkBoxLargeRoom.Location = new System.Drawing.Point(306, 196);
            this.checkBoxLargeRoom.Name = "checkBoxLargeRoom";
            this.checkBoxLargeRoom.Size = new System.Drawing.Size(84, 17);
            this.checkBoxLargeRoom.TabIndex = 12;
            this.checkBoxLargeRoom.Text = "Large Room";
            this.checkBoxLargeRoom.UseVisualStyleBackColor = true;
            // 
            // checkBoxMediumRoom
            // 
            this.checkBoxMediumRoom.AutoSize = true;
            this.checkBoxMediumRoom.Location = new System.Drawing.Point(306, 161);
            this.checkBoxMediumRoom.Name = "checkBoxMediumRoom";
            this.checkBoxMediumRoom.Size = new System.Drawing.Size(94, 17);
            this.checkBoxMediumRoom.TabIndex = 11;
            this.checkBoxMediumRoom.Text = "Medium Room";
            this.checkBoxMediumRoom.UseVisualStyleBackColor = true;
            // 
            // checkBoxSmallRoom
            // 
            this.checkBoxSmallRoom.AutoSize = true;
            this.checkBoxSmallRoom.Location = new System.Drawing.Point(306, 127);
            this.checkBoxSmallRoom.Name = "checkBoxSmallRoom";
            this.checkBoxSmallRoom.Size = new System.Drawing.Size(82, 17);
            this.checkBoxSmallRoom.TabIndex = 10;
            this.checkBoxSmallRoom.Text = "Small Room";
            this.checkBoxSmallRoom.UseVisualStyleBackColor = true;
            // 
            // btnBook
            // 
            this.btnBook.Location = new System.Drawing.Point(306, 265);
            this.btnBook.Name = "btnBook";
            this.btnBook.Size = new System.Drawing.Size(124, 41);
            this.btnBook.TabIndex = 9;
            this.btnBook.Text = "Book";
            this.btnBook.UseVisualStyleBackColor = true;
            this.btnBook.MouseLeave += new System.EventHandler(this.btnBook_MouseLeave_1);
            this.btnBook.MouseHover += new System.EventHandler(this.btnBook_MouseHover);
            // 
            // tabTimetable
            // 
            this.tabTimetable.Controls.Add(this.labelTimetableDate);
            this.tabTimetable.Controls.Add(this.listBoxDisplayBookedTimes);
            this.tabTimetable.Controls.Add(this.dtPickerTimetable);
            this.tabTimetable.Controls.Add(this.btnPersonalTrainerCancelSelected);
            this.tabTimetable.Controls.Add(this.btnPersonalTrainerCreateNote);
            this.tabTimetable.Controls.Add(this.btnCancelSelected);
            this.tabTimetable.Controls.Add(this.btnCreateNote);
            this.tabTimetable.Location = new System.Drawing.Point(4, 22);
            this.tabTimetable.Name = "tabTimetable";
            this.tabTimetable.Padding = new System.Windows.Forms.Padding(3);
            this.tabTimetable.Size = new System.Drawing.Size(550, 436);
            this.tabTimetable.TabIndex = 1;
            this.tabTimetable.Text = "Timetable";
            this.tabTimetable.UseVisualStyleBackColor = true;
            // 
            // labelTimetableDate
            // 
            this.labelTimetableDate.AutoSize = true;
            this.labelTimetableDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTimetableDate.Location = new System.Drawing.Point(86, 94);
            this.labelTimetableDate.Name = "labelTimetableDate";
            this.labelTimetableDate.Size = new System.Drawing.Size(36, 15);
            this.labelTimetableDate.TabIndex = 18;
            this.labelTimetableDate.Text = "Date:";
            // 
            // listBoxDisplayBookedTimes
            // 
            this.listBoxDisplayBookedTimes.FormattingEnabled = true;
            this.listBoxDisplayBookedTimes.Location = new System.Drawing.Point(89, 144);
            this.listBoxDisplayBookedTimes.Name = "listBoxDisplayBookedTimes";
            this.listBoxDisplayBookedTimes.Size = new System.Drawing.Size(200, 160);
            this.listBoxDisplayBookedTimes.TabIndex = 15;
            // 
            // dtPickerTimetable
            // 
            this.dtPickerTimetable.Location = new System.Drawing.Point(89, 112);
            this.dtPickerTimetable.Name = "dtPickerTimetable";
            this.dtPickerTimetable.Size = new System.Drawing.Size(200, 20);
            this.dtPickerTimetable.TabIndex = 14;
            // 
            // btnPersonalTrainerCancelSelected
            // 
            this.btnPersonalTrainerCancelSelected.Location = new System.Drawing.Point(309, 226);
            this.btnPersonalTrainerCancelSelected.Name = "btnPersonalTrainerCancelSelected";
            this.btnPersonalTrainerCancelSelected.Size = new System.Drawing.Size(142, 48);
            this.btnPersonalTrainerCancelSelected.TabIndex = 13;
            this.btnPersonalTrainerCancelSelected.Text = "Cancel Selected";
            this.btnPersonalTrainerCancelSelected.UseVisualStyleBackColor = true;
            // 
            // btnPersonalTrainerCreateNote
            // 
            this.btnPersonalTrainerCreateNote.Location = new System.Drawing.Point(309, 172);
            this.btnPersonalTrainerCreateNote.Name = "btnPersonalTrainerCreateNote";
            this.btnPersonalTrainerCreateNote.Size = new System.Drawing.Size(142, 48);
            this.btnPersonalTrainerCreateNote.TabIndex = 12;
            this.btnPersonalTrainerCreateNote.Text = "Create Note";
            this.btnPersonalTrainerCreateNote.UseVisualStyleBackColor = true;
            // 
            // btnCancelSelected
            // 
            this.btnCancelSelected.Location = new System.Drawing.Point(639, 489);
            this.btnCancelSelected.Name = "btnCancelSelected";
            this.btnCancelSelected.Size = new System.Drawing.Size(142, 48);
            this.btnCancelSelected.TabIndex = 4;
            this.btnCancelSelected.Text = "Cancel Selected";
            this.btnCancelSelected.UseVisualStyleBackColor = true;
            this.btnCancelSelected.Click += new System.EventHandler(this.btnCancelSelected_Click);
            this.btnCancelSelected.MouseLeave += new System.EventHandler(this.btnCancelSelected_MouseLeave);
            this.btnCancelSelected.MouseHover += new System.EventHandler(this.btnCancelSelected_MouseHover);
            // 
            // btnCreateNote
            // 
            this.btnCreateNote.Location = new System.Drawing.Point(134, 489);
            this.btnCreateNote.Name = "btnCreateNote";
            this.btnCreateNote.Size = new System.Drawing.Size(142, 48);
            this.btnCreateNote.TabIndex = 3;
            this.btnCreateNote.Text = "Create Note";
            this.btnCreateNote.UseVisualStyleBackColor = true;
            this.btnCreateNote.Click += new System.EventHandler(this.btnCreateNote_Click);
            this.btnCreateNote.MouseLeave += new System.EventHandler(this.btnCreateNote_MouseLeave);
            this.btnCreateNote.MouseHover += new System.EventHandler(this.btnCreateNote_MouseHover);
            // 
            // tabManageClients
            // 
            this.tabManageClients.Controls.Add(this.labelManageClientsSearch);
            this.tabManageClients.Controls.Add(this.txtBoxManageClientsSearch);
            this.tabManageClients.Controls.Add(this.labelListOfClients);
            this.tabManageClients.Controls.Add(this.listBoxUsers);
            this.tabManageClients.Location = new System.Drawing.Point(4, 22);
            this.tabManageClients.Name = "tabManageClients";
            this.tabManageClients.Padding = new System.Windows.Forms.Padding(3);
            this.tabManageClients.Size = new System.Drawing.Size(550, 436);
            this.tabManageClients.TabIndex = 2;
            this.tabManageClients.Text = "Manage Clients";
            this.tabManageClients.UseVisualStyleBackColor = true;
            // 
            // labelManageClientsSearch
            // 
            this.labelManageClientsSearch.AutoSize = true;
            this.labelManageClientsSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelManageClientsSearch.Location = new System.Drawing.Point(123, 58);
            this.labelManageClientsSearch.Name = "labelManageClientsSearch";
            this.labelManageClientsSearch.Size = new System.Drawing.Size(49, 15);
            this.labelManageClientsSearch.TabIndex = 9;
            this.labelManageClientsSearch.Text = "Search:";
            // 
            // txtBoxManageClientsSearch
            // 
            this.txtBoxManageClientsSearch.Location = new System.Drawing.Point(178, 56);
            this.txtBoxManageClientsSearch.Name = "txtBoxManageClientsSearch";
            this.txtBoxManageClientsSearch.Size = new System.Drawing.Size(241, 20);
            this.txtBoxManageClientsSearch.TabIndex = 8;
            // 
            // labelListOfClients
            // 
            this.labelListOfClients.AutoSize = true;
            this.labelListOfClients.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.labelListOfClients.Location = new System.Drawing.Point(220, 26);
            this.labelListOfClients.Name = "labelListOfClients";
            this.labelListOfClients.Size = new System.Drawing.Size(94, 15);
            this.labelListOfClients.TabIndex = 4;
            this.labelListOfClients.Text = "List of Clients";
            // 
            // listBoxUsers
            // 
            this.listBoxUsers.FormattingEnabled = true;
            this.listBoxUsers.Location = new System.Drawing.Point(132, 87);
            this.listBoxUsers.Name = "listBoxUsers";
            this.listBoxUsers.Size = new System.Drawing.Size(287, 290);
            this.listBoxUsers.TabIndex = 3;
            // 
            // tabCurrentEvents
            // 
            this.tabCurrentEvents.Controls.Add(this.labelCurrentEventsEvents);
            this.tabCurrentEvents.Controls.Add(this.labelEventsDate);
            this.tabCurrentEvents.Controls.Add(this.listBoxDisplayEvents);
            this.tabCurrentEvents.Controls.Add(this.dtPickerEvents);
            this.tabCurrentEvents.Controls.Add(this.btnSignUpForEvent);
            this.tabCurrentEvents.Location = new System.Drawing.Point(4, 22);
            this.tabCurrentEvents.Name = "tabCurrentEvents";
            this.tabCurrentEvents.Padding = new System.Windows.Forms.Padding(3);
            this.tabCurrentEvents.Size = new System.Drawing.Size(550, 436);
            this.tabCurrentEvents.TabIndex = 3;
            this.tabCurrentEvents.Text = "Current Events";
            this.tabCurrentEvents.UseVisualStyleBackColor = true;
            // 
            // labelCurrentEventsEvents
            // 
            this.labelCurrentEventsEvents.AutoSize = true;
            this.labelCurrentEventsEvents.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCurrentEventsEvents.Location = new System.Drawing.Point(178, 106);
            this.labelCurrentEventsEvents.Name = "labelCurrentEventsEvents";
            this.labelCurrentEventsEvents.Size = new System.Drawing.Size(46, 15);
            this.labelCurrentEventsEvents.TabIndex = 25;
            this.labelCurrentEventsEvents.Text = "Events:";
            // 
            // labelEventsDate
            // 
            this.labelEventsDate.AutoSize = true;
            this.labelEventsDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEventsDate.Location = new System.Drawing.Point(178, 60);
            this.labelEventsDate.Name = "labelEventsDate";
            this.labelEventsDate.Size = new System.Drawing.Size(36, 15);
            this.labelEventsDate.TabIndex = 24;
            this.labelEventsDate.Text = "Date:";
            // 
            // listBoxDisplayEvents
            // 
            this.listBoxDisplayEvents.FormattingEnabled = true;
            this.listBoxDisplayEvents.Location = new System.Drawing.Point(181, 124);
            this.listBoxDisplayEvents.Name = "listBoxDisplayEvents";
            this.listBoxDisplayEvents.Size = new System.Drawing.Size(200, 160);
            this.listBoxDisplayEvents.TabIndex = 23;
            // 
            // dtPickerEvents
            // 
            this.dtPickerEvents.Location = new System.Drawing.Point(181, 78);
            this.dtPickerEvents.Name = "dtPickerEvents";
            this.dtPickerEvents.Size = new System.Drawing.Size(200, 20);
            this.dtPickerEvents.TabIndex = 22;
            // 
            // btnSignUpForEvent
            // 
            this.btnSignUpForEvent.Location = new System.Drawing.Point(206, 302);
            this.btnSignUpForEvent.Name = "btnSignUpForEvent";
            this.btnSignUpForEvent.Size = new System.Drawing.Size(141, 34);
            this.btnSignUpForEvent.TabIndex = 21;
            this.btnSignUpForEvent.Text = "Sign up for event";
            this.btnSignUpForEvent.UseVisualStyleBackColor = true;
            // 
            // tabMyEvents
            // 
            this.tabMyEvents.Controls.Add(this.labelMyEventsSearch);
            this.tabMyEvents.Controls.Add(this.txtBoxMyEventsSearch);
            this.tabMyEvents.Controls.Add(this.labelMyEvents);
            this.tabMyEvents.Controls.Add(this.listBoxMyEvents);
            this.tabMyEvents.Location = new System.Drawing.Point(4, 22);
            this.tabMyEvents.Name = "tabMyEvents";
            this.tabMyEvents.Padding = new System.Windows.Forms.Padding(3);
            this.tabMyEvents.Size = new System.Drawing.Size(550, 436);
            this.tabMyEvents.TabIndex = 4;
            this.tabMyEvents.Text = "My Events";
            this.tabMyEvents.UseVisualStyleBackColor = true;
            // 
            // labelMyEventsSearch
            // 
            this.labelMyEventsSearch.AutoSize = true;
            this.labelMyEventsSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMyEventsSearch.Location = new System.Drawing.Point(129, 51);
            this.labelMyEventsSearch.Name = "labelMyEventsSearch";
            this.labelMyEventsSearch.Size = new System.Drawing.Size(49, 15);
            this.labelMyEventsSearch.TabIndex = 12;
            this.labelMyEventsSearch.Text = "Search:";
            // 
            // txtBoxMyEventsSearch
            // 
            this.txtBoxMyEventsSearch.Location = new System.Drawing.Point(184, 49);
            this.txtBoxMyEventsSearch.Name = "txtBoxMyEventsSearch";
            this.txtBoxMyEventsSearch.Size = new System.Drawing.Size(241, 20);
            this.txtBoxMyEventsSearch.TabIndex = 11;
            // 
            // labelMyEvents
            // 
            this.labelMyEvents.AutoSize = true;
            this.labelMyEvents.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.labelMyEvents.Location = new System.Drawing.Point(228, 19);
            this.labelMyEvents.Name = "labelMyEvents";
            this.labelMyEvents.Size = new System.Drawing.Size(71, 15);
            this.labelMyEvents.TabIndex = 6;
            this.labelMyEvents.Text = "My Events";
            this.labelMyEvents.Click += new System.EventHandler(this.labelMyEvents_Click);
            // 
            // listBoxMyEvents
            // 
            this.listBoxMyEvents.FormattingEnabled = true;
            this.listBoxMyEvents.Location = new System.Drawing.Point(132, 75);
            this.listBoxMyEvents.Name = "listBoxMyEvents";
            this.listBoxMyEvents.Size = new System.Drawing.Size(293, 303);
            this.listBoxMyEvents.TabIndex = 5;
            // 
            // tabManageAccount
            // 
            this.tabManageAccount.Location = new System.Drawing.Point(4, 22);
            this.tabManageAccount.Name = "tabManageAccount";
            this.tabManageAccount.Padding = new System.Windows.Forms.Padding(3);
            this.tabManageAccount.Size = new System.Drawing.Size(550, 436);
            this.tabManageAccount.TabIndex = 5;
            this.tabManageAccount.Text = "Manage Account";
            this.tabManageAccount.UseVisualStyleBackColor = true;
            // 
            // tabReportProblem
            // 
            this.tabReportProblem.Controls.Add(this.labelReportProblemFooter);
            this.tabReportProblem.Controls.Add(this.btnSubmitReport);
            this.tabReportProblem.Controls.Add(this.txtBoxReportProblem);
            this.tabReportProblem.Controls.Add(this.labelReportProblemSubtitle);
            this.tabReportProblem.Controls.Add(this.labelReportProblem);
            this.tabReportProblem.Location = new System.Drawing.Point(4, 22);
            this.tabReportProblem.Name = "tabReportProblem";
            this.tabReportProblem.Padding = new System.Windows.Forms.Padding(3);
            this.tabReportProblem.Size = new System.Drawing.Size(550, 436);
            this.tabReportProblem.TabIndex = 6;
            this.tabReportProblem.Text = "Report Problem";
            this.tabReportProblem.UseVisualStyleBackColor = true;
            // 
            // labelReportProblemFooter
            // 
            this.labelReportProblemFooter.AutoSize = true;
            this.labelReportProblemFooter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelReportProblemFooter.Location = new System.Drawing.Point(48, 302);
            this.labelReportProblemFooter.Name = "labelReportProblemFooter";
            this.labelReportProblemFooter.Size = new System.Drawing.Size(453, 15);
            this.labelReportProblemFooter.TabIndex = 4;
            this.labelReportProblemFooter.Text = "Please remember to be as descriptive as possible about the situation!";
            // 
            // btnSubmitReport
            // 
            this.btnSubmitReport.Location = new System.Drawing.Point(190, 341);
            this.btnSubmitReport.Name = "btnSubmitReport";
            this.btnSubmitReport.Size = new System.Drawing.Size(135, 44);
            this.btnSubmitReport.TabIndex = 3;
            this.btnSubmitReport.Text = "Submit Report";
            this.btnSubmitReport.UseVisualStyleBackColor = true;
            // 
            // txtBoxReportProblem
            // 
            this.txtBoxReportProblem.Location = new System.Drawing.Point(61, 91);
            this.txtBoxReportProblem.Multiline = true;
            this.txtBoxReportProblem.Name = "txtBoxReportProblem";
            this.txtBoxReportProblem.Size = new System.Drawing.Size(408, 194);
            this.txtBoxReportProblem.TabIndex = 2;
            // 
            // labelReportProblemSubtitle
            // 
            this.labelReportProblemSubtitle.Location = new System.Drawing.Point(26, 47);
            this.labelReportProblemSubtitle.Name = "labelReportProblemSubtitle";
            this.labelReportProblemSubtitle.Size = new System.Drawing.Size(498, 32);
            this.labelReportProblemSubtitle.TabIndex = 1;
            this.labelReportProblemSubtitle.Text = resources.GetString("labelReportProblemSubtitle.Text");
            // 
            // labelReportProblem
            // 
            this.labelReportProblem.AutoSize = true;
            this.labelReportProblem.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelReportProblem.Location = new System.Drawing.Point(21, 14);
            this.labelReportProblem.Name = "labelReportProblem";
            this.labelReportProblem.Size = new System.Drawing.Size(200, 25);
            this.labelReportProblem.TabIndex = 0;
            this.labelReportProblem.Text = "Report a problem:";
            // 
            // statusStripPersonalTrainer
            // 
            this.statusStripPersonalTrainer.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.personalTrainer_StripLabel,
            this.stripLabelPersonalTrainer});
            this.statusStripPersonalTrainer.Location = new System.Drawing.Point(0, 469);
            this.statusStripPersonalTrainer.Name = "statusStripPersonalTrainer";
            this.statusStripPersonalTrainer.Size = new System.Drawing.Size(568, 22);
            this.statusStripPersonalTrainer.TabIndex = 3;
            this.statusStripPersonalTrainer.Text = "statusStrip1";
            // 
            // personalTrainer_StripLabel
            // 
            this.personalTrainer_StripLabel.Name = "personalTrainer_StripLabel";
            this.personalTrainer_StripLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // stripLabelPersonalTrainer
            // 
            this.stripLabelPersonalTrainer.Name = "stripLabelPersonalTrainer";
            this.stripLabelPersonalTrainer.Size = new System.Drawing.Size(118, 17);
            this.stripLabelPersonalTrainer.Text = "toolStripStatusLabel1";
            // 
            // PersonalTrainer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(568, 491);
            this.Controls.Add(this.statusStripPersonalTrainer);
            this.Controls.Add(this.TabManagement);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "PersonalTrainer";
            this.Text = "LTS | Personal Trainer Portal";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PersonalTrainer_FormClosing);
            this.Load += new System.EventHandler(this.PersonalTrainer_Load);
            this.TabManagement.ResumeLayout(false);
            this.tabBooking.ResumeLayout(false);
            this.tabBooking.PerformLayout();
            this.tabTimetable.ResumeLayout(false);
            this.tabTimetable.PerformLayout();
            this.tabManageClients.ResumeLayout(false);
            this.tabManageClients.PerformLayout();
            this.tabCurrentEvents.ResumeLayout(false);
            this.tabCurrentEvents.PerformLayout();
            this.tabMyEvents.ResumeLayout(false);
            this.tabMyEvents.PerformLayout();
            this.tabReportProblem.ResumeLayout(false);
            this.tabReportProblem.PerformLayout();
            this.statusStripPersonalTrainer.ResumeLayout(false);
            this.statusStripPersonalTrainer.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl TabManagement;
        private System.Windows.Forms.TabPage tabBooking;
        private System.Windows.Forms.TabPage tabTimetable;
        private System.Windows.Forms.TabPage tabManageClients;
        private System.Windows.Forms.TabPage tabCurrentEvents;
        private System.Windows.Forms.TabPage tabMyEvents;
        private System.Windows.Forms.TabPage tabManageAccount;
        private System.Windows.Forms.Label labelListOfClients;
        private System.Windows.Forms.ListBox listBoxUsers;
        private System.Windows.Forms.Button btnCancelSelected;
        private System.Windows.Forms.Button btnCreateNote;
        private System.Windows.Forms.Label labelMyEvents;
        private System.Windows.Forms.ListBox listBoxMyEvents;
        private System.Windows.Forms.StatusStrip statusStripPersonalTrainer;
        private System.Windows.Forms.ToolStripStatusLabel personalTrainer_StripLabel;
        private System.Windows.Forms.ToolStripStatusLabel stripLabelPersonalTrainer;
        private System.Windows.Forms.Label labelManageClientsSearch;
        private System.Windows.Forms.TextBox txtBoxManageClientsSearch;
        private System.Windows.Forms.Label labelTimetableDate;
        private System.Windows.Forms.ListBox listBoxDisplayBookedTimes;
        private System.Windows.Forms.DateTimePicker dtPickerTimetable;
        private System.Windows.Forms.Button btnPersonalTrainerCancelSelected;
        private System.Windows.Forms.Button btnPersonalTrainerCreateNote;
        private System.Windows.Forms.Label labelMyEventsSearch;
        private System.Windows.Forms.TextBox txtBoxMyEventsSearch;
        private System.Windows.Forms.ListBox listBoxDisplayUnbookedTimes;
        private System.Windows.Forms.DateTimePicker dtPickerBooking;
        private System.Windows.Forms.CheckBox checkBoxMUGA;
        private System.Windows.Forms.CheckBox checkBoxLargeRoom;
        private System.Windows.Forms.CheckBox checkBoxMediumRoom;
        private System.Windows.Forms.CheckBox checkBoxSmallRoom;
        private System.Windows.Forms.Button btnBook;
        private System.Windows.Forms.Label labelBookingDate;
        private System.Windows.Forms.Label labelCurrentEventsEvents;
        private System.Windows.Forms.Label labelEventsDate;
        private System.Windows.Forms.ListBox listBoxDisplayEvents;
        private System.Windows.Forms.DateTimePicker dtPickerEvents;
        private System.Windows.Forms.Button btnSignUpForEvent;
        private System.Windows.Forms.TabPage tabReportProblem;
        private System.Windows.Forms.Label labelReportProblemFooter;
        private System.Windows.Forms.Button btnSubmitReport;
        private System.Windows.Forms.TextBox txtBoxReportProblem;
        private System.Windows.Forms.Label labelReportProblemSubtitle;
        private System.Windows.Forms.Label labelReportProblem;
    }
}