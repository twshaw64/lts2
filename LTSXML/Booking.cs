﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace LTSXML
{
    public abstract class Booking :IComparable<Booking>
    {
        #region Private Member Variables
        private int m_BookingID;
        private DateTime m_BookingDate;
        private string m_UserFirstName;
        private string m_UserSurname;
        private string m_MemberUsername;
        #endregion

        #region Fields
        public int BookingID
        {
            get { return m_BookingID; }
            set { m_BookingID = value; }
        }
        public DateTime BookingDate
        {
            get { return m_BookingDate; }
            set { m_BookingDate = value; }
        }

        /// <summary>
        /// The first name of the person making the booking
        /// </summary>
        public string UserFirstName
        {
            get { return m_UserFirstName; }
            set { m_UserFirstName = value; }
        }

        /// <summary>
        /// The surname of the person making the booking
        /// </summary>
        public string UserSurname
        {
            get { return m_UserSurname; }
            set { m_UserSurname = value; }
        }

       /// <summary>
       /// The username of the person making the booking
       /// </summary>
        public string MemberUsername
        {
            get { return m_MemberUsername; }
            set { m_MemberUsername = value; }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor for the base class
        /// </summary>
        /// <param name="pBookingID"></param>
        /// <param name="pBookingDate"></param>
        /// <param name="pFirstname">First name of the person making the booking</param>
        /// <param name="pSurname">Surname of the person making the booking</param>
        /// <param name="pUsername">Username of the person making the booking</param>
        protected Booking(int pBookingID,DateTime pBookingDate, string pFirstname, string pSurname, string pUsername)
        {
            BookingID = pBookingID;
            BookingDate = pBookingDate;
            UserFirstName = pFirstname;
            UserSurname = pSurname;
            MemberUsername = pUsername;
        }
        #endregion

        #region XML
        #region ID Number Management
        public static int NextID
        {
            get
            {
                string filename = StorageLocation.Bookings;
                XDocument bookingsFile = XDocument.Load(filename);
                int output = int.Parse(bookingsFile.Root.Element("nextID").Value);
                IncrementID(output);
                return output;
            }
        }

        private static void IncrementID(int pOriginal)
        {
            string filename = StorageLocation.Bookings;
            XDocument bookingsFile = XDocument.Load(filename);
            int newID = pOriginal+ 1;
            bookingsFile.Root.Element("nextID").Value = newID.ToString();
            bookingsFile.Save(filename);
        }

        #endregion
        #endregion

        #region IComparable
        public int CompareTo(Booking other)
        {
            return BookingDate.CompareTo(other.BookingDate);
        }

        #endregion

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(BookingDate.ToShortDateString());
            builder.Append(": ");
            return builder.ToString();
        }

    }








}
