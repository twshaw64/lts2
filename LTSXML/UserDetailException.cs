﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LTSXML
{
    public class UserDetailException : Exception
    {
        public UserDetailException() : base("One or more of the values entered are incorrect") { }
        public UserDetailException(string pMessage) : base(pMessage) { }
    }
}
